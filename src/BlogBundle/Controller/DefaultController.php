<?php

namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
      return $this->render('@Blog/Default/index.html.twig');
    }

    public function helloAction()
    {
        return $this->render('@Blog/Default/hello.html.twig');
    }
}
